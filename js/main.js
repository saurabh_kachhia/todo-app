/**
 * @fileoverview File for all comman functions.
 * @author saurabh.kachhia@intimetec.com (Saurabh Kachhia)
 */

"use strict";

 /**
 * Get all the items of 'todo' array.
 * @return {array} Array of 'todo' items.
 */
function getTodos() {
    var todos = new Array();
    var todosItem = localStorage.getItem('todo');
    if (todosItem !== null) {
        todos = JSON.parse(todosItem); 
    }
    return todos;
}


/**
 * Get all the items of 'trash' array.
 * @return {array} Array of 'trash' items.
 */
function getTrash() {
    var trash = new Array();
    var trashItem = localStorage.getItem('trash');
    if (trashItem !== null) {
        trash = JSON.parse(trashItem); 
    }
    return trash;
}


 /**
 * Get all the items of 'todoList' array.
 * @return {array} Array of 'todoList' items.
 */
function getTodosList() {
    var todosList = new Array();
    var todosListItem = localStorage.getItem('todoList');
    if (todosListItem !== null) {
        todosList = JSON.parse(todosListItem); 
    }
    return todosList;
}


 /**
 * Get all the items of 'Images' array.
 * @return {array} Array of 'Images' items.
 */
function getImages() {
    var images = new Array();
    var image = localStorage.getItem('images');
    if (image !== null) {
        images = JSON.parse(image); 
    }
    return images;
}


 /**
 * Get all the items of 'trashImages' array.
 * @return {array} Array of 'trashImages' items.
 */
function getTrashImages() {
    var trashImages = new Array();
    var trashImage = localStorage.getItem('trashImages');
    if (trashImage !== null) {
        trashImages = JSON.parse(trashImage); 
    }
    return trashImages;
}


/**
 * Generate current date-time and format it.
 * @return {string} Formated date in {dd mm, hh:mm} format.
 */
function getCurrentDate() {
	var months = new Array();
		months[0] = "January";
		months[1] = "February";
		months[2] = "March";
		months[3] = "April";
		months[4] = "May";
		months[5] = "June";
		months[6] = "July";
		months[7] = "August";
		months[8] = "September";
		months[9] = "October";
		months[10] = "November";
		months[11] = "December";
	
	var currentDate = new Date(); 
	var formatedDateTime = currentDate.getDate()
				+ " "
                + months[currentDate.getMonth()]  
                + ", "  
                + currentDate.getHours() + ":"  
                + currentDate.getMinutes() ;
				
	return formatedDateTime;
}


/**
 * Remove a item from 'trash' item of localStorage.
 */
function removeFromTrash() {
    var id = this.getAttribute('id');

	var trash = getTrash();
    trash.splice(id, 1);
    localStorage.setItem('trash', JSON.stringify(trash));
 
    showTrash();
    return false;
}


/**
 * Restore a item from 'trash'.
 */
function restore() {
	var parentDiv = this.parentElement.parentElement;
	var id = parentDiv.getAttribute('id');
	var type = parentDiv.getAttribute('data-type');

	var trash = getTrash();
	var todos = getTodos();
	var todosList = getTodosList();
	var image = getImages();
	var trashImage = getTrashImages();
	
	var i;
	for(i =0; i<trash.length; i++) {
		if((trash[i].id == id)&&(trash[i].type == type)) {
			break;
		}
	}

	var flag = 0;
	for(var j=0; j<trashImage.length; j++) {
		if(trashImage[j].id==id) {
			flag = 1;
			break;
		}
	}
	
	if(type=='todo') {
		todos.push(trash[i]);
		localStorage.setItem('todo',JSON.stringify(todos));
		
		if(flag == 1) {
		image.push(trashImage[j]);
		localStorage.setItem('images',JSON.stringify(image));
		}
		
	}
	if(type=='todoList') {
		todosList.push(trash[i]);
		localStorage.setItem('todoList',JSON.stringify(todosList))
	}
	
	trashImage.splice(j,1);
	localStorage.setItem('trashImages',JSON.stringify(trashImage));
	
	trash.splice(i,1);
	localStorage.setItem('trash',JSON.stringify(trash));
	
	showTrash();
	return false;	
}


/**
 * Show all trash item from 'trash' item of localStorage.
 */
function showTrash() {
    var trash = getTrash();
 
    var html = '<div>';
    for(var i=0; i<trash.length; i++) {
		if(trash[i].type == 'todoList') {
			var li = '';
			for(var j =0; j<(trash[i].value).length; j++) {
				li+= '<li>'+trash[i].value[j]+'</li>';
			}
		
			html += '<div class="dynamicDiv" id="' + trash[i].id + '" data-type="'+trash[i].type+'">' 
					+'<div style="min-height: 120px;">'
						+ '<h3 class="" id="toDoTitle' + i  + '">'+trash[i].title+'</h3>' 
						+ '<ul>'
						+ li	
						+ '</ul>'
					+'</div>'
					+'<div>'
						+ '<i class="remove fa fa-trash-o fa-lg" id="' + i  + '"></i>'
						+ '<i class="restore fa fa-reply fa-lg" id="editButton' + i  + '"></i>'
						+ '<div> <i class="fa fa-clock-o" aria-hidden="true"></i> '+trash[i].dateTime+'</div>'
					+'</div>'
				+ '</div>';
		}
		if(trash[i].type == 'todo') {
			html += '<div class="dynamicDiv" id="' + trash[i].id + '" data-type="'+trash[i].type+'">' 
					+'<div style="min-height: 120px;">'
						+ '<h3 class="" id="toDoTitle' + i  + '">'+trash[i].title+'</h3>' 
						+ '<span class="" id="toDoMessage' + i  + '">'+ trash[i].value+'</span>' 
					+'</div>'
					+'<div>'
						+ '<i class="remove fa fa-trash-o fa-lg" id="' + i  + '"></i>'
						+ '<i class="restore fa fa-reply fa-lg" id="editButton' + i  + '"></i>'
						+ '<div> <i class="fa fa-clock-o" aria-hidden="true"></i> '+trash[i].dateTime+'</div>'
					+'</div>'
				+ '</div>';
		}
    };
    html += '</div>';
 
    document.getElementById('trashItem').innerHTML = html;
	
	var removeButtons = document.getElementsByClassName('remove');
    for (var i=0; i < removeButtons.length; i++) {
        removeButtons[i].addEventListener('click', removeFromTrash);
    };
	
	var restoreButtons = document.getElementsByClassName('restore');
    for (var i=0; i < restoreButtons.length; i++) {
        restoreButtons[i].addEventListener('click', restore);
    };
	
	$(function() {
		$( ".dynamicDiv" ).draggable();
	});
}


  function handleFileSelect(evt) {
	var parentDiv = this.parentElement.parentElement;
	var id = parentDiv.getAttribute('id');
	var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
		// Only process image files.
		if (!f.type.match('image.*')) {
			continue;
		}

		var reader = new FileReader();

		// Closure to capture the file information.
		reader.onload = (function(theFile) {
        return function(e) {
			// Render thumbnail.
			var span = document.createElement('span');
			span.innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
            
			var output = parentDiv.getElementsByTagName('output')[0];
			output.insertBefore(span, null);
			var images = getImages();
			var update = 0;
	
			for(var i =0; i<images.length; i++) {
				if(images[i].id == id) {
					images[i].image=e.target.result;
					update = 1;
					break;
				}
			}
	
			if(update == 0) {
				images.push(
				{
					id: id,
					image: e.target.result
				});
				localStorage.setItem('images', JSON.stringify(images));
			}
			update = 0;
        };
    })(f);

		// Read in the image file as a data URL.
		reader.readAsDataURL(f);
    }
}

var filename = window.location.pathname.split('/').pop()

if(filename == 'trash.html') {
	showTrash(); 
}