/**
 * @fileoverview Utilities for handling CRUD operation of ToDo List.
 * @author saurabh.kachhia@intimetec.com (Saurabh Kachhia)
 */

"use strict";

/**
 * Add a new TextBox.
 */
function addTextbox() {
	var addTaskDiv = document.getElementById('addTask');
	
	var input = document.createElement("input");
	input.type = "text";
	input.className = "form-control dynamicTextBox";
	addTaskDiv.appendChild(input);
}


/**
 * Add a ToDo List in 'todoList' item of localStorage.
 */
function add() {
	var todoListArray = new Array();
	
	var parentDiv = document.getElementById('addTask');
	
	var todoList = document.getElementsByClassName('dynamicTextBox');
	for(var i =0; i<todoList.length; i++) {
		if(todoList[i].value != "") {
			todoListArray.push(todoList[i].value);
			todoList[i].value = "";
		}
		if(i>0) {
			parentDiv.removeChild(document.getElementsByClassName('dynamicTextBox')[i--]); //Removing Dynamically added textboxs.
		}
			
	}
	
	if(todoListArray.length <= 0) {
		alert("Please enter atleast one list item");
		return false;
	}
	
	var listCounter = localStorage.getItem('listCounter'); // Initialize counter variable to provide ID to new ToDo List.
	if(listCounter == null) {
		localStorage.setItem('listCounter',0);
	}
	listCounter = Number(listCounter)+1;
	localStorage.setItem('listCounter',listCounter);
	
	var taskTitle = document.getElementById('taskTitle').value;
	document.getElementById('taskTitle').value = "";
	var todoList = getTodosList();
		todoList.push(
			{
				id: listCounter,
				title: taskTitle, 
				value: todoListArray,
				dateTime: getCurrentDate(),
				type: 'todoList'
			});
    localStorage.setItem('todoList', JSON.stringify(todoList));
	
	showList();
}


/**
 * Remove a ToDo List from 'todoList' item of localStorage
 * and add removed item to 'trash' item of localStorage.
 */
function remove() {
	var id = this.getAttribute('id');
	
	var trash = getTrash();
	var todoList = getTodosList();

	trash.push(todoList[id]);
	localStorage.setItem('trash', JSON.stringify(trash));
	
	todoList.splice(id,1);
	localStorage.setItem('todoList',JSON.stringify(todoList));
	showList();
}

/**
 * Allow user to edit a ToDo List from 'todoList' item of localStorage.
 */
function edit() {
	var parentDiv = this.parentElement.parentElement;
	var childDiv = parentDiv.getElementsByTagName('div')[0];
	var ul = parentDiv.getElementsByTagName('ul')[0];
	var li = parentDiv.getElementsByTagName('li');
	
	for(var i=0; i<li.length; i++) {
		var input = document.createElement('input');
		input.type = "text";
		input.className = "form-control";
		input.value = li[i].innerText;
		childDiv.appendChild(input);
	}
	
	var done = this.parentElement.getElementsByClassName('done')[0];
	done.addEventListener('click',save);

	this.style.display = 'none';
	done.style.display = 'block';
	ul.style.display = 'none';	
}

/**
 * Save edited ToDo List in 'todoList' item of localStorage.
 */
function save() {
	var parentDiv = this.parentElement.parentElement;
	var id = parentDiv.getAttribute('id');
	var todoList = getTodosList();
	
	var input = parentDiv.getElementsByTagName('input');
	
	var tempArray = new Array();	
	for(var i=0; i<input.length; i++) {
		tempArray.push(input[i].value);
	}
	
	for(var i=0; i<todoList.length; i++) {
		if(todoList[i].id == id ) {
			todoList[i].value = tempArray;
			break;
		}
	}
	
	localStorage.setItem('todoList',JSON.stringify(todoList));
	showList();
}


/**
 * Show all ToDo List from 'todoList' item of localStorage.
 */
function showList() {
	var todoList = getTodosList();
	
	var html = '<div>';
	
    for(var i=0; i<todoList.length; i++) {
		var li = '';
		for(var j =0; j<(todoList[i].value).length; j++) {
			li+= '<li>'+todoList[i].value[j]+'</li>';
		}
		
    html += '<div class="dynamicDiv" id="' + todoList[i].id + '">' 
				+'<div style="min-height: 120px;">'
					+ '<h3 class="" id="toDoTitle' + i  + '">'+todoList[i].title+'</h3>' 
					+ '<ul>'
					+ li	
					+ '</ul>'
				+'</div>'
				+'<div>'
					+'<i class="done fa fa-check fa-lg" style="display:none;" id="' + i  + '"></i>'
					+'<i class="remove fa fa-trash-o fa-lg" id="' + i  + '"></i>'
					+'<i class="edit fa fa-pencil fa-lg" id="editButton' + i  + '"></i>'
					+'<div> <i class="fa fa-clock-o" aria-hidden="true"></i> '+todoList[i].dateTime+'</div>'
				+'</div>'
			+ '</div>';
    };
    html += '</div>';
 
    document.getElementById('todos').innerHTML = html;
	
	var removeButton = document.getElementsByClassName('remove');
	for (var i=0; i<removeButton.length; i++) {
		removeButton[i].addEventListener('click',remove);
	}
	
	var editButton = document.getElementsByClassName('edit');
	for(var i=0; i<editButton.length; i++) {
		editButton[i].addEventListener('click',edit);
	}
	
	$(function() {
		$( ".dynamicDiv" ).draggable();
	});
}

showList();