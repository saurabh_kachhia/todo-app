/**
 * @fileoverview Utilities for handling CRUD operation of Notes.
 * @author saurabh.kachhia@intimetec.com (Saurabh Kachhia)
 */
 
"use strict";

/**
 * Add a note in 'todo' item of localStorage.
 */
function add() {
    var taskTitle = document.getElementById('taskTitle').value;
	var taskValue = document.getElementById('taskValue').value;

	if((taskTitle=="") &&(taskValue=="")) {
		alert("Please enter either title or message.")
		return false;
	}
 
	var counter = localStorage.getItem('counter'); // Initialize counter variable to provide ID to new Note.
	if(counter == null) {
		localStorage.setItem('counter',0);
	}
	counter = Number(counter)+1;
	localStorage.setItem('counter',counter);
 
    var todos = getTodos();
		todos.push(
			{
				id: counter,
				title: taskTitle, 
				value: taskValue,
				dateTime: getCurrentDate(),
				type: 'todo'
			});
    localStorage.setItem('todo', JSON.stringify(todos));
 
	document.getElementById('taskTitle').value = "";
	document.getElementById('taskValue').value = "";
    show();
    return false;
}


/**
 * Remove a note from 'todo' item of localStorage
 * and add removed item to 'trash' item of localStorage.
 */
function remove() {
    var id = this.getAttribute('id');
	var parentDivId = this.parentElement.parentElement.getAttribute('id');
	
    var todos = getTodos();
	var trash = getTrash();
	var images = getImages();
	var trashImages = getTrashImages();
	
	trash.push(todos[id]);
	localStorage.setItem('trash', JSON.stringify(trash));
	
	
	for(var i=0; i<images.length; i++) {
		
		if(images[i].id == parentDivId) {
			trashImages.push(images[i]);
			
			localStorage.setItem('trashImages',JSON.stringify(trashImages));
			images.splice(i,1);
			
			localStorage.setItem('images', JSON.stringify(images));
			break;
		}
	}
	
    todos.splice(id, 1);
    localStorage.setItem('todo', JSON.stringify(todos));
 
    show();
    return false;
}


/**
 * Allow user to edit a note from 'todo' item of localStorage.
 */
function edit() {
    var id = this.getAttribute('id');

	var title = this.parentElement.parentElement.getElementsByTagName('h3')[0];
	var message = this.parentElement.parentElement.getElementsByTagName('span')[0];
	
	var titleText = title.innerText;
	var messageText = message.innerText;
	
	title.style.display='none';
	message.style.display='none';
	
	var titleTextBox = this.parentElement.parentElement.getElementsByTagName('input')[0];
	var messageTextArea = this.parentElement.parentElement.getElementsByTagName('textArea')[0];
	
	titleTextBox.style.display = 'block';
	messageTextArea.style.display = 'block';
	
	titleTextBox.value=titleText;
	messageTextArea.value=messageText;
 
    return false;
}

/**
 * Save edited note in 'todo' item of localStorage.
 */
function save() {
	var titleTextBox = this.parentElement.getElementsByTagName('input')[0];
	
	var title = this.parentElement.getElementsByTagName('h3')[0];
	var message = this.parentElement.getElementsByTagName('span')[0];
		
	var id = this.parentElement.parentElement.getAttribute('id');
	var todos = getTodos();
	
	for(var i =0; i<todos.length; i++) {
		if(todos[i].id == id) {
			todos[i].title=titleTextBox.value;
			todos[i].value=this.value; 
			todos[i].dateTime=getCurrentDate();
			break;
		}
	}
	
	localStorage.setItem('todo', JSON.stringify(todos));
	
	show();
}


/**
 * Show all notes from 'todo' item of localStorage.
 */
function show() {
    var todos = getTodos();
	var images = getImages();
	var temp;
  

         // document.getElementById('list').insertBefore(span, null);
    var html = '<div>';
    for(var i=0; i<todos.length; i++) {							
        html += '<div class="dynamicDiv" id="' + todos[i].id + '" data-type="'+todos[i].type+'">' 
					+'<div style="min-height: 120px;">'
						+ '<h3 class="" id="toDoTitle' + i  + '">'+todos[i].title+'</h3>' 
						+ '<span class="" id="toDoMessage' + i  + '">'+ todos[i].value+'</span>' 
						+ '<input type="text" class="form-control" id ="toDoTitleTextBox' + i + '" style="display:none;width:100%;"></textarea>'
						+ '<textarea class="dynamicTextArea form-control" id ="toDoMessageTextArea' + i + '" style="display:none;width:100%;"></textarea>'
						+ '<output id="list"></output>'
					+'</div>'
					+'<div>'
						+'<input type="file" class="file" id="files" name="files[]" />'
						+'<i class="remove fa fa-trash-o fa-lg" id="' + i  + '"></i>'
						+'<i class="edit fa fa-pencil fa-lg" id="editButton' + i  + '"></i>'
						+'<div> <i class="fa fa-clock-o" aria-hidden="true"></i> '+todos[i].dateTime+'</div>'
					+'</div>'
				+ '</div>';
    };
    html += '</div>';
 
    document.getElementById('todos').innerHTML = html;
 
    var removeButtons = document.getElementsByClassName('remove');
    for (var i=0; i < removeButtons.length; i++) {
        removeButtons[i].addEventListener('click', remove);
    };
	
	var editButtons = document.getElementsByClassName('edit');
    for (var i=0; i < editButtons.length; i++) {
        editButtons[i].addEventListener('click', edit);
    };
	
	var dynamicTextArea = document.getElementsByClassName('dynamicTextArea');
    for (var i=0; i < dynamicTextArea.length; i++) {
        dynamicTextArea[i].addEventListener('blur', save);
    };
	
	var fileUpload = document.getElementsByClassName('file');
    for (var i=0; i < fileUpload.length; i++) {
        fileUpload[i].addEventListener('change', handleFileSelect);
    };
	
	showImages();
	
	$(function() {
		$( ".dynamicDiv" ).draggable();
	});
}

document.getElementById('add').addEventListener('click', add);
show(); 

 
function showImages() {
	var images = getImages();
	for(var i =0; i<images.length; i++) {
		var id = images[i].id;
		var imageOutput = document.getElementsByTagName('div').namedItem(id).getElementsByTagName('output')[0];
		imageOutput.innerHTML = '<img src="'+images[i].image+'" height="100px" width="100px" />';
	}	
}